import "./App.css";

import { useState, useEffect } from "react";

function App() {
	const [ note, setNote ] = useState("");
	
	const storage = window.localStorage;
	
	const saveToLocalStorage = () => {
		storage.setItem('text', JSON.stringify({ text: note }));
	};
	
	const contentHandler = (e) => {
		setNote(e.target.value);
	};
	
	const clearTextArea = () => {
		storage.clear('text');
		document.querySelector('textarea').value = "";
	};
	
	useEffect(() => {
		
			const content = storage.getItem('text');
			const initial = content !== null ? JSON.parse(content).text : "";
			return setNote(initial);
			
		}, []);
	
  return (
    <div className="App">
      <div className="box">
        <div className="field">
          <div className="control">
            <textarea className="textarea is-large" placeholder="Notes..." value={note} onChange={contentHandler} />
          </div>
        </div>
        <button className="button is-large is-primary is-active" onClick={saveToLocalStorage}>Save</button>
        <button className="button is-large" onClick={clearTextArea}>Clear</button>
      </div>
    </div>
  );
}

export default App;
